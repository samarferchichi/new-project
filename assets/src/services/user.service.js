import axios from 'axios';
import authHeader from './auth-header';
import AuthService from "../services/auth.service";

const API_URL = 'http://127.0.0.1:8000/api/';

const currentUser = AuthService.getCurrentUser();
class UserService {

    getOtherUsers() {
        return axios.get(API_URL + 'notification/users', { headers: authHeader() });
    }

    // sendInvitationUsers() {
    //     return axios.get(API_URL + 'send-invitation', { headers: authHeader() });
    // }

//   getPublicContent() {
//     return axios.get(API_URL + 'all');
//   }

//   getUserBoard() {
//     return axios.get(API_URL + 'user', { headers: authHeader() });
//   }

//   getModeratorBoard() {
//     return axios.get(API_URL + 'mod', { headers: authHeader() });
//   }

//   getAdminBoard() {
//     return axios.get(API_URL + 'admin', { headers: authHeader() });
//   }
}

export default new UserService();