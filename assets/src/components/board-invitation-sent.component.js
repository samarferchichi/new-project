import React, { Component } from "react";
import authHeader from '../services/auth-header';
import axios from "axios";

const API_URL = 'http://127.0.0.1:8000/api/';

export default class BoardInvitationsSent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: ""
    };
  }

  componentDidMount() {
    axios.post(API_URL + 'notification/invitations-sent', { headers: authHeader() })
    .then(res => {
      console.log(res)       
    })
  }

  render() {
    return (
      <div className="container">
        <header className="jumbotron">
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">UserName</th>
              <th scope="col">Email</th>
              <th scope="col">Status</th>
            </tr>
          </thead>
          <tbody>
          {Object.keys(this.state.content).map((keyName, i) => (
            <tr key={i}>
              <th scope="row"> {this.state.content[keyName].id} </th>
              <td> {this.state.content[keyName].username} </td>
              <td> {this.state.content[keyName].email} </td>
              <td> "accepté" ou "annulé" </td>
            </tr>
          ))}
          </tbody>
        </table>
      
        </header>
      </div>
    );
  }
}