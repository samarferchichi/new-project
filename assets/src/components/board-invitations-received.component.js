import React, { Component } from "react";

import UserService from "../services/user.service";

export default class BoardInvitationsReceived extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: ""
    };
  }

  componentDidMount() {
    UserService.getOtherUsers().then(
      response => {
        this.setState({
          content: response.data
        });
      },
      error => {
        this.setState({
          content:
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString()
        });
      }
    );
  }

  render() {
    var names = this.state.content;
    return (
      <div className="container">
        <header className="jumbotron">
        <table className="table table-striped">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">UserName</th>
              <th scope="col">Email</th>
              <th scope="col">Status</th>
            </tr>
          </thead>
          <tbody>
          {Object.keys(this.state.content).map((keyName, i) => (
            <tr key={i}>
              <th scope="row"> {this.state.content[keyName].id} </th>
              <td> {this.state.content[keyName].username} </td>
              <td> {this.state.content[keyName].email} </td>
              <td> 
                  <button type="button" class="btn btn-success">Accepte</button>
                  <button type="button" class="btn btn-danger">Danger</button> 
              </td>
            </tr>
          ))}
          </tbody>
        </table>
      
        </header>
      </div>
    );
  }
}